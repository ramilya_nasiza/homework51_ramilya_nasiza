import React, { Component } from 'react';
import './App.css';
import avengers from './avengers.jpg';
import passengers from './passengers.jpeg';
import seven from './seven.jpeg';


const Filmcard = (props) => {
  return(
    <div className="card">
      <img className="card-img-top" src={props.pic} alt=""/>
        <div className="card-body">
          <p className="card-text">Name of film: {props.name}</p>
          <p className="card-text">Date of issue: {props.year}</p>
        </div>
    </div>
  );
};

const films = (
    <React.Fragment>
      <Filmcard pic={avengers} name='The Avengers' year='2010'/>
      <Filmcard pic={passengers} name='Passengers' year='2016'/>
      <Filmcard pic={seven} name='Seven' year='1995'/>
    </React.Fragment>
);


class App extends Component {
  render() {
    return (films);
  }
}

export default App;
