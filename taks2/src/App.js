import React, { Component } from 'react';
import './App.css';
import slide1 from './slide-1.jpg';



const NavList = (props) => {
  return(
      <li className="nav-item">
        <a className="nav-link" href="#">{props.name}</a>
      </li>
  );
};


const Header = () => {
  return(
      <nav className="navbar navbar-expand-lg navbar-dark bg-nav">
        <div className="container-fluid limit-width">
          <a className="navbar-brand" href="#">Thunder</a>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                  aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNavDropdown">
            <ul className="main-nav navbar-nav ml-auto">
              <NavList name='home'/>
              <NavList name='portfolio'/>
              <NavList name='contact'/>
            </ul>
          </div>
        </div>
      </nav>
  );
};

const Mainpic = (props) => {
  return(
      <div className="carousel-block">
        <div id="main-carousel" className="carousel slide">
          <div className="carousel-inner">
            <div className="carousel-item active">
              <div className="container-fluid limit-width"></div>
              <div className="carousel-bg-img"></div>
              <img className="d-block w-100" src={props.img} alt="First slide"/>
            </div>
          </div>
        </div>
      </div>
  );
};

const Item = (props) => {
  return(
      <div className="col-12 col-md-4 text-center advantage px-3">
        <i className={props.icon}></i>
        <h4>{props.title}</h4>
        <p>{props.text}</p>
      </div>
  );
};

const Content = () => {
  return(
      <div className="advantages-block">
        <div className="container-fluid limit-width">
          <div className="row padding">
            <Item icon='far fa-file-alt pic' title='powerfull' text='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam nec dolor vitae neque semper aliquet sed et nibh. Cras quis libero quis urna commodo vulputate'/>
            <Item icon='far fa-chart-bar pic' title='creative' text='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam nec dolor vitae neque semper aliquet sed et nibh. Cras quis libero quis urna commodo vulputate'/>
            <Item icon='fas fa-globe pic' title='localized' text='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam nec dolor vitae neque semper aliquet sed et nibh. Cras quis libero quis urna commodo vulputate'/>
          </div>
        </div>
      </div>
  );
};

const Footer = (props) =>{
  return(
      <div className="footer">
        <div className="container-fluid limit-width">
          <p>{props.txt}</p>
        </div>
      </div>
  );
};

const site = (
    <React.Fragment>
      <Header/>
      <Mainpic img={slide1}/>
      <Content/>
      <Footer txt='COPYRIGHT 2013 - THUNDER'/>
    </React.Fragment>
);

class App extends Component {
  render() {
    return (site);
  }
}

export default App;
